/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DeliverySchedule } from './delivery-schedule.entity';
import { Deliveryman } from '../deliveryman/deliveryman.entity';

@Injectable()
export class DeliveryScheduleService {
  constructor(
    @InjectRepository(DeliverySchedule)
    private readonly deliveryScheduleRepository: Repository<DeliverySchedule>,
    @InjectRepository(Deliveryman)
    private readonly deliverymanRepository: Repository<Deliveryman>,
  ) {}

  async validateSupervisor(deliveryman_id, supervisor_NIK: string): Promise<boolean> {
    const deliveryman = await this.deliverymanRepository.findOne({ where: { NIK: deliveryman_id }});
    if (!deliveryman || deliveryman.NIKSupervisor != supervisor_NIK) {
      return false;
    }
    return true;
  }

  async addSchedule(deliveryman_id, status: string, name: string, gender: string, phone: string, location_latitude: number, location_longitude: number, supervisor_NIK: string): Promise<DeliverySchedule> {
    const isSupervisorValid = await this.validateSupervisor(deliveryman_id, supervisor_NIK);
    if (!isSupervisorValid) {
      throw new Error('Supervisor validation failed');
    }
  
    const schedule = new DeliverySchedule();
    schedule.id = deliveryman_id;
    schedule.status = status;
    schedule.name = name;
    schedule.gender = gender;
    schedule.phone = phone;
    schedule.supervisor_NIK = supervisor_NIK;
    schedule.destination_latitude = location_latitude;
    schedule.destination_longitude = location_longitude;
    return this.deliveryScheduleRepository.save(schedule);
  }

  async updateLocationAndStatus(
    id: number,
    latitude: number,
    longitude: number,
    status: string,
  ): Promise<any> {
    const deliveryman = await this.deliveryScheduleRepository.findOne({ where: { id }});
    if (!deliveryman) {
      throw new NotFoundException('Deliveryman not found');
    }

    deliveryman.destination_latitude = latitude;
    deliveryman.destination_longitude = longitude;
    deliveryman.status = status;

    await this.deliveryScheduleRepository.save(deliveryman);

    return { message: 'Location and status updated successfully' };
  }
}