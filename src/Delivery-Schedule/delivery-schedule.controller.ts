/* eslint-disable prettier/prettier */
import { Controller, Post, Body, HttpException, HttpStatus, Patch, Param } from '@nestjs/common';
import { DeliveryScheduleService } from './delivery-schedule.service';

@Controller('delivery-schedule')
export class DeliveryScheduleController {
  constructor(private readonly deliveryScheduleService: DeliveryScheduleService) {}

  @Post()
  async addSchedule(
    @Body('deliveryman_id') deliveryman_id: number,
    @Body('supervisor_NIK') supervisor_NIK: string,
    @Body('status') status: string,
    @Body('name') name: string,
    @Body('gender') gender: string,
    @Body('phone') phone: string,
    @Body('destination_latitude') destination_latitude: number,
    @Body('destination_longitude') destination_longitude: number,
  ): Promise<any> {
    const isValid = await this.deliveryScheduleService.validateSupervisor(deliveryman_id, supervisor_NIK);
    if (!isValid) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    return this.deliveryScheduleService.addSchedule(deliveryman_id, status, name, gender, phone, destination_latitude, destination_longitude, supervisor_NIK);
  }

  @Patch(':id/update-location-status')
  async updateLocationAndStatus(
    @Param('id') id: number,
    @Body('latitude') latitude: number,
    @Body('longitude') longitude: number,
    @Body('status') status: string,
  ): Promise<any> {
    return this.deliveryScheduleService.updateLocationAndStatus(id, latitude, longitude, status);
  }
}
