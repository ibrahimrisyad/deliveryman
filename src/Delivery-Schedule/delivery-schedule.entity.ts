/* eslint-disable prettier/prettier */

import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity()
export class DeliverySchedule {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  status: string;

  @Column()
  name: string;

  @Column()
  gender: string;

  @Column()
  phone: string;

  @Column()
  NIK: string;

  @Column()
  supervisor_NIK: string;

  @Column({ type: 'decimal', precision: 9, scale: 6 })
  destination_latitude: number;

  @Column({ type: 'decimal', precision: 9, scale: 6 })
  destination_longitude: number;

}
