/* eslint-disable prettier/prettier */
import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { LoggerService } from './logger.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(private readonly loggerService: LoggerService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const { method, originalUrl, body } = req;
    const requestBody = JSON.stringify(body);
    
    await this.loggerService.logRequest(method, originalUrl, requestBody, res.statusCode);

    next();
  }
}

