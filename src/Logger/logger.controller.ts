/* eslint-disable prettier/prettier */
import { Controller, Get } from '@nestjs/common';
import { LoggerService } from './logger.service';
import { Logger } from './logger.entity';

@Controller('logger')
export class LoggerController {
  constructor(private readonly loggerService: LoggerService) {}

  @Get()
  async getAllLogs(): Promise<Logger[]> {
    return this.loggerService.getAllLogs();
  }
}
