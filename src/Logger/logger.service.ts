/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Logger } from './logger.entity';

@Injectable()
export class LoggerService {
  constructor(
    @InjectRepository(Logger)
    private readonly loggerRepository: Repository<Logger>,
  ) {}

  async logRequest(method: string, url: string, requestBody: string, status: number): Promise<void> {
    const logEntry = new Logger();
    logEntry.method = method;
    logEntry.url = url;
    logEntry.requestBody = requestBody;
    logEntry.status = status;

    await this.loggerRepository.save(logEntry);
  }

  async getAllLogs(): Promise<Logger[]> {
    return this.loggerRepository.find();
  }
}
