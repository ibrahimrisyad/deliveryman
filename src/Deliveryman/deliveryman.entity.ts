/* eslint-disable prettier/prettier */
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Deliveryman {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  NIK: string;

  @Column()
  NIKSupervisor: string;

  @Column()
  nama: string;

  @Column()
  umur: number;

  @Column()
  gender: string;

  @Column()
  telp: string;
  
}


