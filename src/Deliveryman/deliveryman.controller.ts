/* eslint-disable prettier/prettier */
import { Controller, Post, Body} from '@nestjs/common';
import { DeliverymanService } from './deliveryman.service';
import { Deliveryman } from './deliveryman.entity';

@Controller('deliverymen')
export class DeliverymanController {
  constructor(private readonly deliverymanService: DeliverymanService) {}

  @Post()
  async createDeliveryman(@Body() data: Partial<Deliveryman>): Promise<Deliveryman> {
    return this.deliverymanService.createDeliveryman(data);
  }
}
