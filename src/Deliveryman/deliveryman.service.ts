/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Deliveryman } from './deliveryman.entity';

@Injectable()
export class DeliverymanService {
  constructor(
    @InjectRepository(Deliveryman)
    private deliverymanRepository: Repository<Deliveryman>,
  ) {}

  async createDeliveryman(data: Partial<Deliveryman>): Promise<Deliveryman> {
    const deliveryman = this.deliverymanRepository.create(data);
    return this.deliverymanRepository.save(deliveryman);
  }

  
}
