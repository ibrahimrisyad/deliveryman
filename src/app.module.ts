/* eslint-disable prettier/prettier */
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { UsersController } from './users/user.controller';
import { UsersService } from './users/user.service';
import { User } from './users/user.entity';
import { DeliverymanController } from './deliveryman/deliveryman.controller';
import { DeliverymanService } from './deliveryman/deliveryman.service';
import { Deliveryman } from './deliveryman/deliveryman.entity';
import { DeliverySchedule } from './delivery-schedule/delivery-schedule.entity';
import { DeliveryScheduleController } from './delivery-schedule/delivery-schedule.controller';
import { DeliveryScheduleService } from './delivery-schedule/delivery-schedule.service';
import { LoggerMiddleware } from './Logger/logger.middleware';
import { Logger } from './Logger/logger.entity';
import { LoggerService } from './Logger/logger.service';
import { LoggerController } from './Logger/logger.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'packet_monitoring',
      synchronize: true,
      logging: true,
      entities: [User, Deliveryman, DeliverySchedule, Logger],
    }),
    TypeOrmModule.forFeature([User, Deliveryman, DeliverySchedule, Logger]),
  ],
  controllers: [UsersController, DeliverymanController, DeliveryScheduleController, LoggerController],
  providers: [UsersService, DeliverymanService, DeliveryScheduleService,LoggerService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
